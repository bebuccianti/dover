import Vue from 'vue'
import VueI18n from 'vue-i18n'

Vue.use(VueI18n)

const messages = {
  en: {
    dashboard: {
      title: 'Profile'
    },
    user: {
      fullname: 'Fullname',
      save: 'Save changes',
      language: 'Language'
    },
    search: {
      title: 'Search',
      see: 'See project',
      deadline: 'Deadline',
    },
    projects: {
      title: 'My projects',
      open: 'Open project',
      newP: 'New Project',
    },
    newProject: {
      title: 'Title',
      description: 'Description',
      hint: 'A detailed description help to get better proposals',
      date: 'Date',
      close: 'Close',
      save: 'Save'
    },
    login: {
      register: 'Register',
      login: 'Login',
      confirm: 'Confirm password'
    }
  },
  es: {
    dashboard: {
      title: 'Perfil'
    },
    user: {
      fullname: 'Nombre completo',
      save: 'Guardar cambios',
      language: 'Idioma'
    },
    search: {
      title: 'Buscador',
      see: 'Ver proyecto',
      deadline: 'Fecha límite'
    },
    projects: {
      title: 'Mis proyectos',
      open: 'Abrir proyecto',
      newP: 'Nuevo Proyecto'
    },
    newProject: {
      title: 'Título',
      description: 'Descripción',
      hint: 'Una descripción detallada te puede ayudar a conseguir mejores propuestas',
      date: 'Fecha límite',
      close: 'Cancelar',
      save: 'Guardar'
    },
    login: {
      register: 'Registrarse',
      login: 'Entrar',
      confirm: 'Confirmar password'
    }
  }
}

const i18n = new VueI18n({
    locale: 'es',
    messages
})

export default i18n
