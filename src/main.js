import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import store from './store'
import router from './router'
import i18n from './plugins/i18n'

Vue.config.productionTip = false

new Vue({
  store,
  router,
  render: h => h(App),
  i18n,
}).$mount('#app')
