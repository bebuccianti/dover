import Vue from 'vue'
import Router from 'vue-router'
import store from './store'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('./views/Login.vue')
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: () => import('./views/Dashboard.vue'),
      beforeEnter(to, from, next) {
        store.dispatch('auth/authenticate').then(() => {
          next()
        }).catch(() => {
          next('/login')
        })
      },
    },
    {
      path: '/search',
      name: 'search',
      component: () => import('./views/Search.vue'),
      beforeEnter(to, from, next) {
        store.dispatch('auth/authenticate').then(() => {
          next()
        }).catch(() => {
          next('/login')
        })
      },
    },
    {
      path: '/projects',
      name: 'projects',
      component: () => import('./views/Projects.vue'),
      beforeEnter(to, from, next) {
        store.dispatch('auth/authenticate').then(() => {
          next()
        }).catch(() => {
          next('/login')
        })
      },
    },
    {
      path: '/projects/:id',
      name: 'project',
      component: () => import('./views/Project.vue'),
      beforeEnter(to, from, next) {
        store.dispatch('auth/authenticate').then(() => {
          next()
        }).catch(() => {
          next('/login')
        })
      },
    },
  ]
})
